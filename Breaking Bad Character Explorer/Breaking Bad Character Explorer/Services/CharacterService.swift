//
//  CharacterService.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import Foundation
import Combine

struct CharacterService {
    func fetchCharacters() -> AnyPublisher<[CharacterModel], Error> {
        let url = URL(string: "https://breakingbadapi.com/api/characters")!
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: [CharacterModel].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
