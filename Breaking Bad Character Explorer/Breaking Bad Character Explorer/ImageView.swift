//
//  ImageView.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import Foundation
import SwiftUI
import Nuke

struct ImageView: View {
    let url: URL?

    @StateObject private var image = FetchImage()

    var body: some View {
        ZStack {
            Rectangle().fill(Color.gray)
            image.view?
                .resizable()
                .aspectRatio(contentMode: .fill)
        }
        .onAppear {
            image.priority = .normal
            image.load(url)
        }
        .onDisappear {
            image.priority = .low
        }
    }
}

