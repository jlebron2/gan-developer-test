//
//  CharacterListViewModel.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import SwiftUI
import Combine

class CharacterListViewModel: ObservableObject {
    @Published var characters = [CharacterModel]()
    @Published var searchText: String = ""
    @Published var selectedFilters: Set<Int> = Set([])
    private let service: CharacterService
    private var cancellables = Set<AnyCancellable>()
    
    init(service: CharacterService) {
        self.service = service
        
        service.fetchCharacters()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                case .finished:
                    print("publisher finished")
                case .failure(let error):
                    print(error)
                }
            }, receiveValue: { [weak self] (characters) in
                self?.characters = characters
            })
            .store(in: &cancellables)
    }
    
    func getFilteredCharacters() -> [CharacterModel] {
        return characters.filter {
            let matchesSearch = searchText.isEmpty ? true :  $0.name.lowercased().contains(searchText.lowercased())
            let matchesSeason = selectedFilters.isEmpty ? true : Set($0.appearance).intersection(selectedFilters).count > 0
            
            return matchesSearch && matchesSeason
        }
    }
}
