//
//  CharacterModel.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import Foundation

struct CharacterModel: Codable {
    var char_id: Int
    var name: String
    var birthday: String
    var occupation: [String]
    var img: String
    var status: String
    var nickname: String
    var appearance: [Int]
    var portrayed: String
    var category: String
    var better_call_saul_appearance: [Int]
    
    static var example: CharacterModel {
        CharacterModel(
            char_id: 0,
            name: "Walter White",
            birthday: "09-07-1958",
            occupation: ["High School Chemistry Teacher", "Meth King Pin"],
            img: "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg",
            status: "Presumed dead",
            nickname: "Heisenberg",
            appearance: [1, 2, 3, 4, 5],
            portrayed: "Bryan Cranston",
            category: "Breaking Bad",
            better_call_saul_appearance: []
        )
    }
}
