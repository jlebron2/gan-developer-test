//
//  Breaking_Bad_Character_ExplorerApp.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import SwiftUI

@main
struct Breaking_Bad_Character_ExplorerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
