//
//  FilterView.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/2/21.
//

import SwiftUI

struct FilterView: View {
    @Environment(\.presentationMode) var presentationMode
    @Binding var selectedFilters: Set<Int>
    @State private var tempSelectedFilters: Set<Int>
    private let dismissButtonImage = "xmark"
    private let dismissAccessibilityLabel = "dismiss"
    private let filterTitle = "Filter"
    private let filterTitleAccessibilityId = "filterTitle"
    private let descriptionText = "Filter characters by season appearance:"
    private let resetButtonText = "Reset"
    private let resetButtonAccessibilityId = "reset"
    private let selectedImage = "checkmark.circle.fill"
    private let showResultsButtonText = "See results"
    private let showResultsButtonAccessibilityId = "showResults"
    let seasons = [1,2,3,4,5]
    
    init(selectedFilters: Binding<Set<Int>>) {
        self._selectedFilters = selectedFilters
        self._tempSelectedFilters = State(initialValue: selectedFilters.wrappedValue)
    }
    
    var body: some View {
        VStack(spacing: 50) {
            HStack {
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: dismissButtonImage)
                        .foregroundColor(.red)
                        .font(.title3)
                })
                .accessibilityLabel(dismissAccessibilityLabel)
                
                Spacer()
                
                Text(filterTitle)
                    .bold()
                    .font(.title)
                    .padding()
                    .accessibilityIdentifier(filterTitleAccessibilityId)
                
                Spacer()
                
                Button(action: {
                    tempSelectedFilters = Set([])
                }, label: {
                    Text(resetButtonText)
                        .font(.title3)
                })
                .accessibilityIdentifier(resetButtonAccessibilityId)
            }
            .padding([.leading, .trailing])
            
            
            Text(descriptionText)
                .font(.subheadline)
                .frame(maxWidth:.infinity, alignment: .leading)
                .padding(.leading, 16)
            
            List(seasons, id: \.self) { season in
                Button(action: {
                    if tempSelectedFilters.contains(season) {
                        tempSelectedFilters.remove(season)
                    } else {
                        tempSelectedFilters.insert(season)
                    }
                }) {
                    HStack {
                        Text("Season \(season)")
                            .font(.title3)
                            .padding()
                        
                        Spacer()
                        
                        if tempSelectedFilters.contains(season) {
                            Image(systemName: selectedImage)
                                .foregroundColor(.accentColor)
                                .font(.title2)
                        }
                    }
                }
            }
            
            Spacer()
            
            HStack {
                Button(action: {
                    selectedFilters = tempSelectedFilters
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Text(showResultsButtonText)
                        .font(.title3)
                        .frame(maxWidth: .infinity, maxHeight: 40)
                })
                .background(Color.accentColor)
                .foregroundColor(.white)
                .cornerRadius(4)
                .padding()
                .accessibilityIdentifier(showResultsButtonAccessibilityId)
            }
        }
    }
}

struct FilterView_Previews: PreviewProvider {
    @State static var selectedFilters: Set<Int> = Set([])
    
    static var previews: some View {
        FilterView(selectedFilters: $selectedFilters)
    }
}
