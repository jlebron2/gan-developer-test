//
//  RoundedButtonStyle.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/3/21.
//

import SwiftUI

struct RoundButtonStyle: ButtonStyle {
    private let buttonHeight: CGFloat = 64
    let backgroundColor: Color
    
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .font(.title)
            .frame(width: buttonHeight, height: buttonHeight)
            .foregroundColor(.white)
            .background(backgroundColor)
            .cornerRadius(buttonHeight/2)
            .padding()
            .shadow(color: Color.black.opacity(0.3), radius: 3, x: 3, y: 3)
    }
}
