//
//  ContentView.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import SwiftUI
import SwiftlySearch

struct ContentView: View {
    @StateObject private var characterListViewModel = CharacterListViewModel(service: CharacterService())
    @State var showFilter: Bool = false
    private let navigationTitle = "Character Explorer"
    private let searchPlaceholder = "Search"
    
    var body: some View {
        NavigationView {
            let characters = characterListViewModel.getFilteredCharacters()
            
            List(characters, id: \.char_id) { character in
                NavigationLink(destination: CharacterDetailsView(character: character)) {
                    CharacterListRowView(character: character)
                }
            }
            .navigationTitle(Text(navigationTitle))
            .navigationBarSearch($characterListViewModel.searchText, placeholder: searchPlaceholder)
            .overlay(FilterButton(action: { showFilter = true }), alignment: .bottomTrailing)
        }
        .sheet(isPresented: $showFilter) {
            FilterView(selectedFilters: $characterListViewModel.selectedFilters)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
