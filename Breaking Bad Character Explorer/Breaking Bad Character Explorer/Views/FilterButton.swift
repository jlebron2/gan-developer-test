//
//  FilterButton.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/3/21.
//

import SwiftUI

struct FilterButton: View {
    private let filterButtonImage = "slider.horizontal.3"
    private let filterAccessibilityLabel = "filter"
    let action: () -> Void
    
    var body: some View {
        Button(action: action,
               label: { Image(systemName: filterButtonImage) }
        )
        .buttonStyle(RoundButtonStyle(backgroundColor: .accentColor))
        .accessibilityLabel(filterAccessibilityLabel)
    }
}

struct FilterButton_Previews: PreviewProvider {
    static var previews: some View {
        FilterButton(action: {})
    }
}
