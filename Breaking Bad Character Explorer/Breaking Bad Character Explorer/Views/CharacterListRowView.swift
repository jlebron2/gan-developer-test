//
//  CharacterListRowView.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/3/21.
//

import SwiftUI

struct CharacterListRowView: View {
    let character: CharacterModel
    
    var body: some View {
        HStack(spacing: 12) {
            ImageView(url:URL(string: character.img))
                .frame(width: 60.0, height: 60.0, alignment: .top)
                .clipShape(Circle())
            
            VStack(spacing: 4) {
                Text(character.name)
                    .font(.headline)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                Text("Played by \(character.portrayed)")
                    .font(.subheadline)
                    .italic()
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
        }
    }
}

struct CharacterListRowView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterListRowView(character: CharacterModel.example)
    }
}
