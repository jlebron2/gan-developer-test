//
//  CharacterInfoView.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import SwiftUI

struct CharacterInfoView: View {
    var character: CharacterModel
    private let titlePaddingLeading: CGFloat = 20
    private let titlePaddingTop: CGFloat = 4
    private let textPaddingLeading: CGFloat = 40
    private let backgroundMaxHeight = UIScreen.main.bounds.size.height * 0.7
    private let infoViewPaddingTop: CGFloat = -40
    private let infoViewPaddingHorizontal: CGFloat = 24
    private let backgroundImage = "bb_background"
    private let birthdayTitle = "Birthday"
    private let birthdayAccessibilityId = "characterBirthday"
    private let occupationTitle = "Occupation"
    private let occupationAccessibilityId = "characterOccupation"
    private let statusTitle = "Status"
    private let statusAccessibilityId = "characterStatus"
    private let playedByTitle = "Played By"
    private let seasonAppearancesTitle = "Season Appearances"
    
    var body: some View {
        ZStack {
            Image(backgroundImage)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(maxWidth: .infinity,
                       maxHeight: backgroundMaxHeight,
                       alignment: .bottom)
                .clipped()
            
            VStack {
                Group {
                    Text(birthdayTitle)
                        .bold()
                        .font(.title3)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.top, 16)
                        .padding(.leading, titlePaddingLeading)

                    Text("\(character.birthday)")
                        .font(.subheadline)
                        .italic()
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, textPaddingLeading)
                        .accessibilityIdentifier(birthdayAccessibilityId)
                }
                
                Group {
                    Text(occupationTitle)
                        .bold()
                        .font(.title3)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, titlePaddingLeading)
                        .padding(.top, titlePaddingTop)
                    
                    Text("\(character.occupation.joined(separator: "\n"))")
                        .font(.subheadline)
                        .italic()
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, textPaddingLeading)
                        .accessibilityIdentifier(occupationAccessibilityId)
                }
                
                Group {
                    Text(statusTitle)
                        .bold()
                        .font(.title3)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, titlePaddingLeading)
                        .padding(.top, titlePaddingTop)
                    
                    Text("\(character.status)")
                        .font(.subheadline)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, textPaddingLeading)
                        .accessibilityIdentifier(statusAccessibilityId)
                }
                
                Group {
                    Text(playedByTitle)
                        .bold()
                        .font(.title3)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, titlePaddingLeading)
                        .padding(.top, titlePaddingTop)
                    
                    Text("\(character.portrayed)")
                        .font(.subheadline)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, textPaddingLeading)
                }
                
                Group {
                    Text(seasonAppearancesTitle)
                        .bold()
                        .font(.title3)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, titlePaddingLeading)
                        .padding(.top, titlePaddingTop)
                    
                    let appearances = character.appearance.map { String($0) }.joined(separator: ", ")
                    Text("\(appearances)")
                        .font(.subheadline)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, textPaddingLeading)
                        .padding(.bottom, 16)
                }
                
                Spacer()
            }
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .fill(Color.white)
            )
            .padding(.horizontal, infoViewPaddingHorizontal)
            .padding(.top, infoViewPaddingTop)
        }
    }
}

struct CharacterInfoView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterInfoView(character: CharacterModel.example)
    }
}
