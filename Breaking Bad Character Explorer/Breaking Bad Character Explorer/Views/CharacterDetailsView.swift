//
//  CharacterDetailsView.swift
//  Breaking Bad Character Explorer
//
//  Created by JL on 10/1/21.
//

import SwiftUI

struct CharacterDetailsView: View {
    @Environment(\.presentationMode) var presentationMode
    var character: CharacterModel
    private let namePaddingBottom: CGFloat = 72
    private let nicknamePaddingBottom: CGFloat = 40
    private let logoHeight: CGFloat = 180
    private let logoWidth: CGFloat = 200
    private let imageMaxWidth = UIScreen.main.bounds.size.width
    private let imageMaxHeight = UIScreen.main.bounds.size.height * 0.6
    private let imagePaddingBottom: CGFloat = -40
    private let characterNameAccessibilityId = "characterName"
    private let characterNickameAccessibilityId = "characterNickame"
    private let backButtonImage = "arrow.left"
    private let backButtonAccessibilityLabel = "back"
    
    var body: some View {
        NavigationView() {
            VStack {
                ZStack(alignment: .bottom) {
                    ImageView(url:URL(string: character.img))
                        .frame(maxWidth: imageMaxWidth,
                               maxHeight: imageMaxHeight,
                               alignment: .top)
                        .clipped()
                        .padding(.bottom, imagePaddingBottom)
                    
                    Text(character.name)
                        .bold()
                        .font(.largeTitle)
                        .foregroundColor(.white)
                        .background(Color.black)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .padding(.bottom, namePaddingBottom)
                        .accessibilityIdentifier(characterNameAccessibilityId)
                    
                    Text("a/k/a/ \(character.nickname)")
                        .font(.title2)
                        .foregroundColor(.white)
                        .background(Color.black)
                        .frame(maxWidth: .infinity, alignment: .center)
                        .padding(.bottom, nicknamePaddingBottom)
                        .accessibilityIdentifier(characterNickameAccessibilityId)
                }
                
                CharacterInfoView(character: character)
            }
            .edgesIgnoringSafeArea(.top)
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)
    }
    
    var backButton : some View {
        Button(action: {
            presentationMode.wrappedValue.dismiss()
        },label: {
            Image(systemName: backButtonImage)
                .frame(width: 40, height: 40)
                .background(Color.accentColor)
                .foregroundColor(.white)
                .font(.title2)
                .cornerRadius(20)
                .accessibilityLabel(backButtonAccessibilityLabel)
        })
    }
}

struct CharacterDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterDetailsView(character: CharacterModel.example)
    }
}

