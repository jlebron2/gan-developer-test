//
//  Breaking_Bad_Character_ExplorerUITests.swift
//  Breaking Bad Character ExplorerUITests
//
//  Created by JL on 10/1/21.
//

import XCTest

class Breaking_Bad_Character_ExplorerUITests: XCTestCase {
    let app = XCUIApplication()
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        app.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCharacterDetailView() throws {
        // Check Walter White row exists
        let firstRow = app.tables.cells.element(boundBy: 0)
        XCTAssert(firstRow.waitForExistence(timeout: 0.5))
        
        firstRow.tap()
        
        // Check character name text
        let characterName = app.staticTexts["characterName"]
        XCTAssert(characterName.waitForExistence(timeout: 0.5))
        XCTAssertEqual(characterName.label, "Walter White")
        
        // Check character nickname text
        let characterNickname = app.staticTexts["characterNickname"]
        XCTAssert(characterNickname.exists)
        XCTAssert(characterNickname.label.contains("Heisenberg"))
        
        // Check character birthday text
        let characterBirthday = app.staticTexts["characterBirthday"]
        XCTAssert(characterBirthday.exists)
        XCTAssertEqual(characterBirthday.label, "09-07-1958")
        
        // Check character occupation text
        let characterOccupation = app.staticTexts["characterOccupation"]
        XCTAssert(characterOccupation.exists)
        XCTAssert(characterOccupation.label.contains("Meth King Pin"))
        
        // Check character status text
        let characterStatus = app.staticTexts["characterStatus"]
        XCTAssert(characterStatus.exists)
        XCTAssertEqual(characterStatus.label, "Presumed dead")
    }

    func testFilterView() throws {
        // Check filter button exists
        let filterButton = app.buttons["filter"]
        XCTAssert(filterButton.exists)
        
        filterButton.tap()
        
        // Check filter title exists
        let filterTitle = app.staticTexts["filterTitle"]
        XCTAssert(filterTitle.waitForExistence(timeout: 0.5))
        
        // Check filter title text
        XCTAssertEqual(filterTitle.label, "Filter")
        
        // Check show results button exists
        let showResultsButton = app.buttons["showResults"]
        XCTAssert(showResultsButton.exists)
        
        // Check reset button exists
        let resetButton = app.buttons["reset"]
        XCTAssert(resetButton.exists)
        
        // Check dismiss button exists
        let dismissButton = app.buttons["dismiss"]
        XCTAssert(dismissButton.exists)
    }
    
    func testValidSearch() throws {
        let searchBar = app.navigationBars.firstMatch.children(matching: .searchField).firstMatch
        XCTAssert(searchBar.exists)
        
        searchBar.tap()
        searchBar.typeText("tortuga")
        
        let tortugaRow = app.tables.cells.element(boundBy: 0)
        XCTAssert(tortugaRow.exists)
        
        tortugaRow.tap()
        
        // Check character name text
        let characterName = app.staticTexts["characterName"]
        XCTAssert(characterName.waitForExistence(timeout: 0.5))
        XCTAssertEqual(characterName.label, "Tortuga")
    }
    
    func testInvalidSearch() throws {
        let searchBar = app.navigationBars.firstMatch.children(matching: .searchField).firstMatch
        XCTAssert(searchBar.exists)
        
        searchBar.tap()
        searchBar.typeText("asdf")
        
        let firstRow = app.tables.cells.element(boundBy: 0)
        XCTAssertFalse(firstRow.exists)
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
